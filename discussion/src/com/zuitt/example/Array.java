package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    //[SECTION] Java Collection
    //are a single unit objects
    //Useful for manipulating relevant pieces of data that can be used in different situations and commonly used in loops
    public static void main (String[] args){
        //[SECTION] Array
        //In Java, arrays are container of values of the same type given a predefined amount/number of values.
        //Java arrays are more rigid, once the size and data type are defined, they can no longer be changed.

        //Syntax for Array Declaration:
            //datatype[] identifier = new dataType[numOfElements];
            //"[]" indicates that the data type should be able to hold multiple values.
            //"new" keyword is used for non-primitive data types to tell Java to create the said variable.
            //"numOfElements" will tell how many elements the array can hold
        //Array declaration:
        //if we are going to declare an array of int[], the default value of the elements will all be 0.
        //for array of String[], the default value of all the elements will be null

        int[] intArray = new int[5];

        //to initialize the value of our elements inside the array, we are going to use the index
        intArray[0] = 200;
        intArray[1] = 122;
        intArray[2] = 322;
        intArray[3] = 566;

        //this will just print out the memory address of the array
        System.out.println(intArray);

        //to print out the intArray, we need to import the Arrays Class and use the .toString() method.
        //this method will convert the array as a string in the terminal
        System.out.println(Arrays.toString(intArray));

        //Declaration and Initialization of an Array
        //Syntax:
            //dataType[] identifier = {elementA, elementB, ...};
            //the compiler automatically specifies the size by counting the number of the elements during the initialization
        String[] names = {"John", "Ian", "Earl", "Chris", "Seth"};
        //names[5] = "Theo";// ArrayIndexOutOfBoundsException
        System.out.println(Arrays.toString(names));

        //Sample java array methods
        //Sort method:
        Arrays.sort(intArray);
        System.out.println ("After sorting "+ Arrays.toString(intArray));
        Arrays.sort(names);
        System.out.println ("After sorting "+ Arrays.toString(names));

        //Multidimensional Array
        //A two-dimensional array can be described by two lengths nested within each other, like a matrix
            //first length is row, second length is the column, arrayName[][]
        //[['as', 'bs', 'ds'], ['as', 'bs', 'ds'], ['as', 'bs', 'ds']]
        String[][] classroom = new String[3][3];

        //[['as', 'bs', 'ds'], ['as', 'bs', 'ds']]
        String[][] classroom2 = new String[2][3];

        //[['as', 'bs'], ['as', 'bs'],['as', 'bs']]
        String[][] classroom3 = new String[3][2];

        //1st row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        //2nd row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Junjun";
        classroom[1][2] = "Jobert";

        //3rd row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofey";

        //This is ONLY applicable for TWO-dimensional arrays
        System.out.println(Arrays.deepToString(classroom));

        //Note: In Java, the size of the array cannot be modified. If there is a need to add or remove elements, you need to create a new array.

        //[Section] Arraylist
            //resizable arrays, wherein elements can be added or removed whenever it is needed.
            //Syntax:
                //ArrayList<T> identifier = new ArrayList<T>();
                //"<T>" - ise used to specify that the list can only have one type of object in a collection
                //Arraylist cannot hold primitive data types, "java wrapper classes" provide a way to use these types as objects.
                //in short, Object version of primitive data type with methods.

            //Declaration of an ArrayList
                //Example of usage of primitive data type as a generic in the ArrayList
                //ArrayList<int> numbers = new ArrayList<int>(); //type argument cannot be of primitive type

                //Usage of Integer
                ArrayList<Integer> numbers = new ArrayList<Integer>();

                //Add elements
                //arrayName.add(element);
                numbers.add(1);
                System.out.println(numbers);

                //access an element from an ArrayList
                //arrayListName.get(index);

                System.out.println(numbers.get(0));

                //Declaration with Initialization
                ArrayList<String> students = new ArrayList<String>(Arrays.asList("Lean", "Ian", "Theo"));
                System.out.println(students);
                System.out.println(students.get(0));
                System.out.println(students.get(1));
                System.out.println(students.get(2));

                //add elements on the ArrayList students:
                students.add("Ragnar");
                System.out.println(students);
                //access the elements
                System.out.println(students.get(3));
                //updating an element
                //arrayListName.set(index, updatedElement);
                students.set(3, "Thea");
                System.out.println(students);
                //remove a specific element

                //students.remove("Thea");
                students.remove(1);
                System.out.println(students);
                //getting the size of an arrayList
                System.out.println(students.size());
                //removing all elements
                students.clear();
                System.out.println(students);
                System.out.println(students.size());

                //[Section] Hashmaps
                //most objects in Java are defined and are instantiated of Classes that contains a proper set of properties and methods
                //There might be use cases where it is not appropriate, or you may simply want a collection of data in key-value pairs.
                //in Java, "keys" also referred as "fields" where in the values can be accessed through fields
                //Syntax: Hashmap<dataTypeField, dataTypeValue> identifier = new HashMap <dataTypeField, dataTypeValue>();

                //Declaration of a HasMap
                HashMap<String, String> jobPosition = new HashMap<String, String>();
                System.out.println(jobPosition);

                //Methods in HashMap:
                //Add key-value pair: HashMapName.put(fieldName, fieldValue);
                jobPosition.put("Student", "Brandon");
                System.out.println(jobPosition);
                jobPosition.put("Teacher", "Sir Baldy");
                System.out.println(jobPosition);
                jobPosition.put("Student", "John");
                System.out.println(jobPosition);
                jobPosition.put("Dreamer", "Alice");

                //Accessing Elements - we use fields name because they are unique.
                //hashMapName.get("fieldsName");
                System.out.println(jobPosition.get("Student"));
                System.out.println(jobPosition.get("Teacher"));
                //if the fieldName does not exist, it will return null
                System.out.println(jobPosition.get("Teachers"));

                //Updating Values
                //HashMapName.replace("fieldNametoChange", "newValue");
                jobPosition.replace("Student", "Theo Gomez");
                System.out.println(jobPosition);

                //Removing an Element
                //HashMapName.remove("key");
                jobPosition.remove("Dreamer");
                System.out.println(jobPosition);

                //Retrieve HashMap keys: HashMapName.keySet();
                System.out.println(jobPosition.keySet());

                //Retrieve the value from the HashMap: HashMapName.values();
                System.out.println(jobPosition.values());

                //Remove all the key-value pairs: HashMapName.clear();
                jobPosition.clear();
                System.out.println(jobPosition);

                //Declaration of HashMap with Initialization
                HashMap<String, String> jobPosition2 = new HashMap<String, String>(){
                    {
                        put("Teacher", "Sir Mick");
                        put("Artist", "Jane");
                    }
                };

                System.out.println(jobPosition2);
    }
}
